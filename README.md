# Td1 : Expenses validation

Voici l'application utilisée par la société JeanMich Corporation pour valider les notes de frais de ses employés. 
Cette application a été conçue de manière très chaotique. 
Aucun design n'a été fait et le développeur ne s'est pas soucié du devenir de l'application, la fin de sa mission approchant il ne lui a pas semblé utile de penser à l'avenir de sa "création".
Malgré le fait qu'elle ressemble plus à un tableau de Picasso qu'à la Tour Eiffel, l'application a permis à l'entreprise de répondre à un dispositif légal imposé par l'URSSAF.  

L'entreprise a en effet l'obligation légale de refuser les notes de frais jugées "irraisonnables". L'application ExpenseValidator lui permet depuis 3ans d'extraire les notes de frais invalides.  

Malheureusement pour JeanMich Corporation, un inspecteur URSSAF est venu dans ses locaux pour auditer le système de validation de note de frais et il s'est aperçu d'une erreur sur la validation. 
En plus d'une grosse amende mettant l'entreprise en grande difficulté, l'inspecteur a exigé la correction de l'application ExpenseValidator afin de revenir sur le chemin de la légalité.

C'est donc là que vous intervenez. 

## 0 - Infos pratiques
Pour récupérer les sources de l'application, clonez le repo git suivant :
```
git clone http://gitlab.isima.fr/javapro/td1.git
```
Créez une branche à votre nom :
```
git checkout -b binome1-binome2
```

Commitez et poussez vos modifications régulièrement :
```
git add .
git commit -m "Mon message"
git push
```


## 1 - Repensez la structure de l'application 
Une analyse rapide du code vous permet de vous apercevoir de l'absence de design, de structure de l'application. 
Cela pose un premier problème puisque vous êtes dans l'incapacité de tester ExpenseValidator de manière automatique. 
Des tests unitaires ont bien été rédigés, malheureusement ExpenseValidator est intestable en l'état et la visite de l'inspecteur a montré que c'est un problème mettant en jeux la survie de l'entreprise.  

Afin de pallier à ce problème, analysez le code existant afin de le rendre plus modulaire et donc plus testable. 
Le SRP "Single Responsability Principle" est ici la clé pour repenser la structure de l'application. 


## 2 - Adaptez les tests
Bien, l'application étant plus modulaire donc plus facilement testable et pouvant s'adapter aux évolutions future, il nous faut encore trouver l'erreur ayant amené le redressement de l'URSSAF.  

Pour cela, adaptez les tests à votre application (car oui désormais c'est la vôtre) afin de découvrir le bug. Dans la foulée, corrigez le assurez-vous que tous les tests sont au vert !    

