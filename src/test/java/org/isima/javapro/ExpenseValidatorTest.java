package org.isima.javapro;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.fail;

public class ExpenseValidatorTest {

    @Test
    @DisplayName("Test expense validator with invalid expense")
    void testWithInvalidExpense() {
        fail("Impossible to test it ¯\\_(ツ)_/¯");
    }

    @Test
    @DisplayName("Test expense validator with valid expense")
    void testWithValidExpense() {
        fail("Impossible to test it ¯\\_(ツ)_/¯");
    }

    @Test
    @DisplayName("Retrieve expenses from a remote location")
    void testRetrieveExpenses() {
        fail("Impossible to test it ¯\\_(ツ)_/¯");
    }

    @Test
    @DisplayName("Create expenses from a CSV data")
    void testCreateExpensesFromCsv() {
        fail("Impossible to test it ¯\\_(ツ)_/¯");
    }

    @Test
    @DisplayName("Validate expense's employee exists")
    void testIsEmployeeExists() {
        fail("Impossible to test it ¯\\_(ツ)_/¯");
    }

    @Test
    @DisplayName("Validate if an expense have a legal amount")
    void testHaveLegalAmount() {
        fail("Impossible to test it ¯\\_(ツ)_/¯");
    }

    @Test
    @DisplayName("Validate if an expense respect enterprise policy")
    void testHavePolicyAmount() {
        fail("Impossible to test it ¯\\_(ツ)_/¯");
    }

    @Test
    @DisplayName("Validate if employees expenses respect the month policy")
    void testRespectMonthPolicy() {
        fail("Impossible to test it ¯\\_(ツ)_/¯");
    }

    @Test
    @DisplayName("Append valid expenses to expenses history")
    void testSaveValidExpenses() {
        fail("Impossible to test it ¯\\_(ツ)_/¯");
    }

    @Test
    @DisplayName("Append invalid expenses to expenses history")
    void testSaveInvalidExpenses() {
        fail("Impossible to test it ¯\\_(ツ)_/¯");
    }
}
